var express = require('express');

// Create a router object from the top level express object for performing middleware and routing functions. 
var router = express.Router();

// Good practice to use route method to avoid duplicate route naming and thus typo errors
router.route('/groups')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "aboveCenter",
                            "center",
                            "east",
                            "south",
                            "underCenter",
                            "fixedHeader",
                            "fixedFooter"
                        ]
                    },
                    "header": {
                        "parentid": "home",
                        "placement": "fixedHeader",
                        "type": "header",
                        "data": {
                            "text": "Expense Groups",
                            "name": "Cristiana Man",
                            "icon": "fa fa-bars"
                        }
                    },
                    "footer": {
                        "parentid": "home",
                        "placement": "fixedFooter",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "foot1"
                                }
                            ]
                        }
                    },
                    "iconTest": {
                        "placement": "foot1",
                        "parentid": "footer",
                        "type": "icon",
                        "data": {
                            "icon": "fa fa-plus-circle"
                        }
                    },
                    "verticalLayout": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "layout-vertical",
                        "data": [
                            "business-label-container",
                            "grid1",
                            "private-label-container",
                            "grid2"
                        ]
                    },
                    "grid1": {
                        "placement": "grid1",
                        "parentid": "verticalLayout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "paris-expense-tile-container",
                                "workshop-expense-tile-container",
                                "projectX-expense-tile-container"
                            ],
                            "columns": 2
                        }
                    },
                    "grid2": {
                        "placement": "grid2",
                        "parentid": "verticalLayout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "moscow-expense-tile-container"
                            ],
                            "columns": 2
                        }
                    },
                    "business-label": {
                        "parentid": "verticalLayout",
                        "placement": "business-label-container",
                        "type": "label",
                        "data": {
                            "text": "Business (3)"
                        }
                    },
                    "private-label": {
                        "parentid": "verticalLayout",
                        "placement": "private-label-container",
                        "type": "label",
                        "data": {
                            "text": "Private (1)"
                        }
                    },
                    "paris-expense-tile": {
                        "parentid": "grid1",
                        "placement": "paris-expense-tile-container",
                        "type": "expense-tile",
                        "data": {
                            "icon": "fa-ellipsis-v",
                            "header": "Paris",
                            "serverAction": "gui/globetrotter/summary"
                        }
                    },
                    "workshop-expense-tile": {
                        "parentid": "grid1",
                        "placement": "workshop-expense-tile-container",
                        "type": "expense-tile",
                        "data": {
                            "icon": "fa-ellipsis-v",
                            "header": "Workshop X"
                        }
                    },
                    "projectX-expense-tile": {
                        "parentid": "grid1",
                        "placement": "projectX-expense-tile-container",
                        "type": "expense-tile",
                        "data": {
                            "icon": "fa-ellipsis-v",
                            "header": "Project X"
                        }
                    },
                    "moscow-expense-tile": {
                        "parentid": "grid2",
                        "placement": "moscow-expense-tile-container",
                        "type": "expense-tile",
                        "data": {
                            "icon": "fa-ellipsis-v",
                            "header": "Moscow"
                        }
                    }
                }
            }
        );
    });

router.route('/summary')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "above-center",
                            "center",
                            "east",
                            "south",
                            "underCenter",
                            "header-fixed",
                            "footer-fixed"
                        ]
                    },
                    "headerTest": {
                        "parentid": "home",
                        "placement": "header-fixed",
                        "type": "header",
                        "data": {
                            "text": "Paris",
                            "name": "Expenses",
                            "icon": "fa fa-arrow-left",
                            "serverAction": "expense/groups"
                        }
                    },
                    "footTest": {
                        "parentid": "home",
                        "placement": "footer-fixed",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "foot1"
                                },
                                {
                                    "container": "foot2"
                                },
                                {
                                    "container": "foot3"
                                },
                                {
                                    "container": "foot4"
                                }
                            ]
                        }
                    },
                    "uploadTool": {
                        "placement": "foot1",
                        "parentid": "footTest",
                        "type": "upload",
                        "data": {}
                    },
                    "iconTest2": {
                        "placement": "foot2",
                        "parentid": "footTest",
                        "type": "icon",
                        "data": {
                            "icon": "fa-plus-square",
                            "actions": [
                                {
                                    "serverAction": 'gui/globetrotter/endpoint1'
                                },
                                {
                                    "serverAction": 'gui/globetrotter/endpoint2'
                                },
                                {
                                    "serverAction": 'gui/globetrotter/editexpense'
                                }
                            ]

                        }
                    },
                    "iconTest3": {
                        "placement": "foot3",
                        "parentid": "footTest",
                        "type": "icon",
                        "data": {
                            "icon": "fa-download"
                        }
                    },
                    "iconTest4": {
                        "placement": "foot4",
                        "parentid": "footTest",
                        "type": "icon",
                        "data": {
                            "icon": "fa-paper-plane"
                        }
                    },
                    "tabs": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "Timeline",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Overview"
                                }
                            ]
                        }
                    },
                    "layout-vertical-overview": {
                        "parentid": "tabs",
                        "placement": "tab2",
                        "type": "layout-vertical",
                        "data": [
                            "restaurant-container",
                            "flight-container",
                            "hotel-container",
                            "internet-container",
                            "total-container"
                        ]
                    },
                    "restaurant-overview": {
                        "parentid": "layout-vertical-overview",
                        "placement": "restaurant-container",
                        "type": "overview-expense",
                        "data": {
                            "category": 2,
                            "header": "Restaurant",
                            "currency": "DKK",
                            "amount": 1105
                        }
                    },
                    "flight-overview": {
                        "parentid": "layout-vertical-overview",
                        "placement": "flight-container",
                        "type": "overview-expense",
                        "data": {
                            "category": 2,
                            "header": "Flight",
                            "currency": "DKK",
                            "amount": 1699
                        }
                    },
                    "hotel-overview": {
                        "parentid": "layout-vertical-overview",
                        "placement": "hotel-container",
                        "type": "overview-expense",
                        "data": {
                            "category": 2,
                            "header": "Hotel",
                            "currency": "DKK",
                            "amount": 236
                        }
                    },
                    "internet-overview": {
                        "parentid": "layout-vertical-overview",
                        "placement": "internet-container",
                        "type": "overview-expense",
                        "data": {
                            "category": 2,
                            "header": "Internet",
                            "currency": "DKK",
                            "amount": 299
                        }
                    },
                    "total-bar": {
                        "parentid": "layout-vertical-overview",
                        "placement": "total-container",
                        "type": "total",
                        "data": {
                            "icon": "fa-balance-scale",
                            "header": "Total",
                            "currency": "DKK",
                            "amount": "4868.00"
                        }
                    },
                    "layout-vertical": {
                        "parentid": "tabs",
                        "placement": "tab1",
                        "type": "layout-vertical",
                        "data": [
                            "tivoli-container",
                            "easyjet-container",
                            "hotelconcordia-container",
                            "swiper-container"
                        ]
                    },
                    "tivoli-expense": {
                        "parentid": "layout-vertical",
                        "placement": "tivoli-container",
                        "type": "timeline-expense",
                        "data": {
                            "category": 2,
                            "header": "Tivoli",
                            "date": "June 27, 2016",
                            "currency": "DKK",
                            "amount": 566,
                            "status": 1
                        }
                    },
                    "easyjet-expense": {
                        "parentid": "layout-vertical",
                        "placement": "easyjet-container",
                        "type": "timeline-expense",
                        "data": {
                            "category": 1,
                            "header": "Easy Jet",
                            "date": "June 26, 2016",
                            "currency": "EUR",
                            "amount": 335,
                            "status": 0
                        }
                    },
                    "hotelconcordia-expense": {
                        "parentid": "layout-vertical",
                        "placement": "hotelconcordia-container",
                        "type": "timeline-expense",
                        "data": {
                            "category": 0,
                            "header": "Concordia",
                            "date": "June 27, 2016",
                            "currency": "DKK",
                            "amount": 5236,
                            "status": 2
                        }
                    },
                    "swiper": {
                        "parentid": "layout-vertical",
                        "placement": "swiper-container",
                        "type": "swiper",
                        "data": {
                            "containers": [
                                {
                                    "container": "tivoli-container",
                                    "active": true
                                },
                                {
                                    "container": "options-container"
                                }
                            ],
                            "serverAction" : "gui/globetrotter/receipt"
                        }
                    },
                    "tivoli-expense2": {
                        "parentid": "swiper",
                        "placement": "tivoli-container",
                        "type": "timeline-expense",
                        "data": {
                            "category": 2,
                            "header": "Tivoli",
                            "date": "June 27, 2016",
                            "currency": "DKK",
                            "amount": 566,
                            "status": 1
                        }
                    },
                    "tivoli-expense2": {
                        "parentid": "swiper",
                        "placement": "tivoli-container",
                        "type": "timeline-expense",
                        "data": {
                            "category": 2,
                            "header": "SWIPE THIS",
                            "date": "June 27, 2016",
                            "currency": "DKK",
                            "amount": 566,
                            "status": 1
                        }
                    },
                    "options": {
                        "parentid": "swiper",
                        "placement": "options-container",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "option1",
                                    "type": "options"
                                },
                                {
                                    "container": "option2",
                                    "type": "options"
                                },
                                {
                                    "container": "option3",
                                    "type": "options"
                                }
                            ],
                            "options" : {
                                "height" : "90px"
                            }
                        }
                    },
                    "optionsIcon1": {
                        "placement": "option1",
                        "parentid": "options",
                        "type": "icon",
                        "data": {
                            "icon": "fa-pencil"
                        }
                    },
                    "optionsIcon2": {
                        "placement": "option2",
                        "parentid": "options",
                        "type": "icon",
                        "data": {
                            "icon": "fa-arrows"
                        }
                    },
                    "optionsIcon3": {
                        "placement": "option3",
                        "parentid": "options",
                        "type": "image",
                        "data": {
                            "image": "app/resources/img/trash.svg"
                        }
                    }


                }
            }
        );
    });


router.route('/uploadapproval')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "above-center",
                            "center",
                            "east",
                            "south",
                            "underCenter",
                            "header-fixed",
                            "footer-fixed"
                        ]
                    },
                    "headerTest": {
                        "parentid": "home",
                        "placement": "header-fixed",
                        "type": "header",
                        "data": {
                            "text": "Paris",
                            "name": "Upload receipt",
                            "icon": "fa fa-arrow-left",
                            "serverAction": "expense/paris"
                        }
                    },
                    "footTest": {
                        "parentid": "home",
                        "placement": "footer-fixed",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "foot1",
                                    "type": "error"
                                },
                                {
                                    "container": "foot2",
                                    "type": "ok"
                                }
                            ]
                        }
                    },
                    "trash": {
                        "placement": "foot1",
                        "parentid": "footTest",
                        "type": "image",
                        "data": {
                            "image": "app/resources/img/trash.svg"
                        }
                    },
                    "send": {
                        "placement": "foot2",
                        "parentid": "footTest",
                        "type": "image",
                        "data": {
                            "image": "app/resources/img/arrow.svg"
                        }
                    },
                    "receipt": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "image",
                        "data": {
                            "image": "https://media-cdn.tripadvisor.com/media/photo-s/03/63/af/02/george-s-italian-restaurant.jpg"
                        }
                    }
                }
            }
        );
    });

router.route('/receipt')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "above-center",
                            "center",
                            "east",
                            "south",
                            "underCenter",
                            "header-fixed",
                            "footer-fixed"
                        ]
                    },
                    "headerTest": {
                        "parentid": "home",
                        "placement": "header-fixed",
                        "type": "header",
                        "data": {
                            "text": "Receipt",
                            "name": "Tivoli",
                            "icon": "fa fa-arrow-left",
                            "serverAction": "gui/globetrotter/summary"
                        }
                    },
                    "footTest": {
                        "parentid": "home",
                        "placement": "footer-fixed",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "foot1",
                                    "type": "error"
                                },
                                {
                                    "container": "foot2",
                                    "type": "ok"
                                }
                            ]
                        }
                    },
                    "trash": {
                        "placement": "foot1",
                        "parentid": "footTest",
                        "type": "image",
                        "data": {
                            "image": "app/resources/img/trash.svg"
                        }
                    },
                    "send": {
                        "placement": "foot2",
                        "parentid": "footTest",
                        "type": "icon",
                        "data": {
                            "icon": "fa fa-check"
                        }
                    },
                    "receipt": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "image",
                        "data": {
                            "image": "https://media-cdn.tripadvisor.com/media/photo-s/03/63/af/02/george-s-italian-restaurant.jpg"
                        }
                    }
                }
            }
        );
    });


router.route('/editexpense')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "above-center",
                            "center",
                            "east",
                            "south",
                            "underCenter",
                            "header-fixed",
                            "footer-fixed"
                        ]
                    },
                    "headerTest": {
                        "parentid": "home",
                        "placement": "header-fixed",
                        "type": "header",
                        "data": {
                            "text": "Add expense",
                            "name": "Add new expense manually",
                            "icon": "fa fa-arrow-left",
                            "serverAction": "gui/globetrotter/summary"
                        }
                    },
                    "footTest": {
                        "parentid": "home",
                        "placement": "footer-fixed",
                        "type": "footer",
                        "data": {
                            "containers": [
                                {
                                    "container": "foot1",
                                    "type": "error"
                                },
                                {
                                    "container": "foot2",
                                    "type": "ok"
                                }
                            ]
                        }
                    },
                    "trash": {
                        "placement": "foot1",
                        "parentid": "footTest",
                        "type": "image",
                        "data": {
                            "image": "app/resources/img/trash.svg"
                        }
                    },
                    "send": {
                        "placement": "foot2",
                        "parentid": "footTest",
                        "type": "icon",
                        "data": {
                            "icon": "fa-check"
                        }
                    },
                    "expenseForm": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "category",
                                    "label": "Category",
                                    "value": "1",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "Uncategorized",
                                            "value": "1"
                                        },
                                        {
                                            "label": "Flight",
                                            "value": "2"
                                        },
                                        {
                                            "label": "Restaurant",
                                            "value": "3"
                                        },
                                        {
                                            "label": "Option 3",
                                            "value": "4"
                                        },
                                        {
                                            "label": "Option 4",
                                            "value": "5"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "amount",
                                    "label": "Amount (DKK)",
                                    "value": "",
                                    "type": "number",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "boolExample",
                                    "label": "12?",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "notes",
                                    "label": "Notes",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "attendees",
                                    "label": "Attendees",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options" : {
                                "isHorizontal" : false
                            }
                        }
                    }
                }
            }
        );
    });

router.route('/endpoint1')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        console.log("ENDPOINT 1");
        res.status(200);

    });

router.route('/endpoint2')
// Handler function (middleware system) for get request
    .get(function (req, res) {
        console.log("ENDPOINT 2");
        res.status(200);


    });

module.exports = router;
