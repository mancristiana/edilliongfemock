var express = require('express');

// Create a router object from the top level express object for performing middleware and routing functions. 
var router = express.Router();

router.route('/documents').get(function(req, res) {
        res.status(200);
        res.json(

        );
    });

router.route('/vendors/24688').get(function(req, res) {
        res.status(200);
        res.json(

        );
    });

router.route('/configuration').get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "aboveCenter",
                            "center",
                            "east",
                            "south",
                            "underCenter"
                        ]
                    },
                    "toolbar-id": {
                        "parentid": "home",
                        "placement": "north",
                        "type": "toolbar",
                        "data": {
                            "logo": [
                                {
                                    "id": "logo-placement",
                                    "float": "left"
                                },
                                {
                                    "id": "logout-placement",
                                    "float": "right"
                                },
                                {
                                    "id": "label-placement",
                                    "float": "right"
                                }
                            ],
                            "tools": [
                                {
                                    "id": "tool-right-4",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-3",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-2",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-1",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-left-1",
                                    "float": "left"
                                }
                            ]
                        }
                    },
                    "logo": {
                        "parentid": "toolbar-id",
                        "placement": "logo-placement",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/images/logo-full.png",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "logout-button": {
                        "parentid": "toolbar-id",
                        "placement": "logout-placement",
                        "type": "button",
                        "data": {
                            "text": "Logout",
                            "type": "primary"
                        }
                    },
                    "e-conomic-label": {
                        "parentid": "toolbar-id",
                        "placement": "label-placement",
                        "type": "label",
                        "data": {
                            "text": "Not connected to e-conomic!",
                            "type": "alert",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "label-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-1",
                        "type": "label",
                        "data": {
                            "text": "Cristiana Man",
                            "serverAction": "frontpage/edillion"
                        }
                    },
                    "icon-1-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-2",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/home-new-white.png",
                            "serverAction": "/frontpage"
                        }
                    },
                    "icon-2-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-3",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/customer-white.png"
                        }
                    },
                    "icon-3-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-4",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/setup-white.png",
                            "serverAction": "/frontpage/accountSetup"
                        }
                    },
                    "menu-table": {
                        "placement": "west",
                        "parentid": "home",
                        "type": "table",
                        "data": {
                            "columns": [
                                {
                                    "index": 0,
                                    "header": "",
                                    "sort": "asc",
                                    "show": true
                                }
                            ],
                            "rows": [
                                {
                                    "id": "master",
                                    "cells": [
                                        "Master data"
                                    ]
                                },
                                {
                                    "id": "product",
                                    "cells": [
                                        "Product configuration"
                                    ]
                                },
                                {
                                    "id": "e-conomic",
                                    "cells": [
                                        "e-conomic"
                                    ]
                                },
                                {
                                    "id": "accounting",
                                    "cells": [
                                        "Accounting"
                                    ]
                                },
                                {
                                    "id": "approval",
                                    "cells": [
                                        "Approval"
                                    ],
                                    "selected": true
                                },
                                {
                                    "id": "company",
                                    "cells": [
                                        "Company"
                                    ]
                                },
                                {
                                    "id": "journal",
                                    "cells": [
                                        "Journal"
                                    ]
                                },
                                {
                                    "id": "customer",
                                    "cells": [
                                        "Customer attributes"
                                    ]
                                },
                                {
                                    "id": "newcustomer",
                                    "cells": [
                                        "New customer"
                                    ]
                                }
                            ],
                            "options": {
                                "selectMode": "single",
                                "showHeader": false
                            },
                            "callback": "gfe/configuration"
                        }
                    },
                    "tabs": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "Settings",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Groups"
                                },
                                {
                                    "id": "tab3",
                                    "title": "Create Group"
                                }
                            ],
                            "options": {
                                "alignment": "left"
                            }
                        }
                    },
                    "tab1Layout": {
                        "placement": "tab1",
                        "parentid": "tabs",
                        "type": "layout-vertical",
                        "data": [
                            "label1",
                            "generalProperty",
                            "label2",
                            "documentsProperty",
                            "label3",
                            "defaultsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tab1Label1": {
                        "parentid": "tab1Layout",
                        "placement": "label1",
                        "type": "label",
                        "data": {
                            "text": "General"
                        }
                    },
                    "tab1Label2": {
                        "parentid": "tab1Layout",
                        "placement": "label2",
                        "type": "label",
                        "data": {
                            "text": "Downloads"
                        }
                    },
                    "tab1Label3": {
                        "parentid": "tab1Layout",
                        "placement": "label3",
                        "type": "label",
                        "data": {
                            "text": "Group Defaults"
                        }
                    },
                    "tab1PropertyEditor1": {
                        "placement": "generalProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Enable approval flow",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Approval flow package",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false,
                                    "help": "This is some message."
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1PropertyEditor2": {
                        "placement": "documentsProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Account required",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Payment means required",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Pressure Payment means",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1PropertyEditor3": {
                        "placement": "defaultsProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "field1",
                                    "label": "Forward new documents to",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false,
                                    "help": "This is some message."
                                },
                                {
                                    "id": "field2",
                                    "label": "Forward new documents with problms to",
                                    "value": "1",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field3",
                                    "label": "Default book keeper group",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "Group 1",
                                            "value": "2"
                                        },
                                        {
                                            "label": "Group 2",
                                            "value": "3"
                                        },
                                        {
                                            "label": "Group 3",
                                            "value": "4"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field4",
                                    "label": "Default superior group",
                                    "value": "3",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "Group 1",
                                            "value": "2"
                                        },
                                        {
                                            "label": "Group 2",
                                            "value": "3"
                                        },
                                        {
                                            "label": "Group 3",
                                            "value": "4"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field5",
                                    "label": "Forward rejected documents to",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field6",
                                    "label": "Default duration",
                                    "value": "22",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tab1Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tab1ButtonSave": {
                        "placement": "save",
                        "parentid": "tab1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tab1ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tab1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    },
                    "tab3Layout": {
                        "placement": "tab3",
                        "parentid": "tabs",
                        "type": "layout-vertical",
                        "data": [
                            "createProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tab3PropertyEditor3": {
                        "placement": "createProperty",
                        "parentid": "tab3Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "t3field1",
                                    "label": "Group name",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab3ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tab3Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "create"
                            ],
                            "columns": 1
                        }
                    },
                    "tab3ButtonCreate": {
                        "placement": "create",
                        "parentid": "tab3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Create",
                            "type": "primary"
                        }
                    },
                    "tab2Table": {
                        "placement": "tab2",
                        "parentid": "tabs",
                        "type": "table",
                        "data": {
                            "options": {
                                "selectMode": "single",
                                "showHeader": true
                            },
                            "columns": [
                                {
                                    "index": 0,
                                    "header": "Name",
                                    "sort": "asc",
                                    "show": true
                                },
                                {
                                    "index": 1,
                                    "header": "Members",
                                    "sort": "asc",
                                    "show": false
                                },
                                {
                                    "index": 2,
                                    "header": "Approval limit",
                                    "sort": "asc",
                                    "show": true
                                },
                                {
                                    "index": 3,
                                    "header": "Superior",
                                    "sort": "asc",
                                    "show": true
                                }
                            ],
                            "rows": [
                                {
                                    "id": "one",
                                    "cells": [
                                        "Marketing",
                                        "Peter",
                                        "10.000",
                                        "CEO"
                                    ]
                                },
                                {
                                    "id": "2",
                                    "cells": [
                                        "Development",
                                        "Cristiana, Simon, Mathias, Flaviu",
                                        "5.000",
                                        "CEO"
                                    ]
                                },
                                {
                                    "id": "3",
                                    "cells": [
                                        "CEO",
                                        "Christian",
                                        "No limit",
                                        "No superior"
                                    ]
                                }
                            ],
                            "callback": "gfe/configuration/approval/group"
                        }
                    }
                }
            }
        );
    });

router.route('/configuration/approval/group/one').get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "tabsGroup": {
                        "placement": "east",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "settings",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Members"
                                },
                                {
                                    "id": "tab3",
                                    "title": "Rights"
                                }
                            ],
                            "options": {
                                "alignment": "center"
                            }
                        }
                    },
                    "tabsGroup1Layout": {
                        "placement": "tab1",
                        "parentid": "tabsGroup",
                        "type": "layout-vertical",
                        "data": [
                            "settingsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tabsGroup1PropertyEditor": {
                        "placement": "settingsProperty",
                        "parentid": "tabsGroup1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Name",
                                    "value": "Marketing",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false,
                                    "help": "This is a help message"
                                },
                                {
                                    "id": "project",
                                    "label": "Project",
                                    "value": "Some Fancy Name",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "department",
                                    "label": "Department",
                                    "value": "Marketing",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "bookkeeper",
                                    "label": "Bookkeeper",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "assignproject",
                                    "label": "Auto-assign received docs to project",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "assigndepartment",
                                    "label": "Auto-assign received docs to department",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": false
                            }
                        }
                    },
                    "tabsGroup1ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tabsGroup1Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup1ButtonSave": {
                        "placement": "save",
                        "parentid": "tabsGroup1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tabsGroup1ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tabsGroup1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    },
                    "tabsGroup2Grid": {
                        "placement": "tab2",
                        "parentid": "tabsGroup",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "label1",
                                "removeButton1",
                                "label2",
                                "removeButton2",
                                "label3",
                                "removeButton3",
                                "addMemberButton"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup2Label1": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label1",
                        "type": "label",
                        "data": {
                            "text": "Simon"
                        }
                    },
                    "tabsGroup2Button1": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton1",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2Label2": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label2",
                        "type": "label",
                        "data": {
                            "text": "Mathias"
                        }
                    },
                    "tabsGroup2Button2": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton2",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2Label3": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label3",
                        "type": "label",
                        "data": {
                            "text": "Flaviu"
                        }
                    },
                    "tabsGroup2Button3": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton3",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2ButtonAddMember": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "addMemberButton",
                        "type": "button",
                        "data": {
                            "text": "Add member",
                            "type": "primary"
                        }
                    },
                    "tabsGroup3Layout": {
                        "placement": "tab3",
                        "parentid": "tabsGroup",
                        "type": "layout-vertical",
                        "data": [
                            "rightsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tabsGroup3PropertyEditor": {
                        "placement": "rightsProperty",
                        "parentid": "tabsGroup3Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "forward",
                                    "label": "Forward",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "postpone",
                                    "label": "Postpone",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "bookkeeper",
                                    "label": "Send to bookkeeper",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "approve1",
                                    "label": "Approve (is paid)",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "approve2",
                                    "label": "Approve (to be paid)",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit1",
                                    "label": "Edit Account",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit2",
                                    "label": "Edit payment means",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit3",
                                    "label": "Edit project",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit4",
                                    "label": "Edit department",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "reject",
                                    "label": "Reject",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tabsGroup3ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tabsGroup3Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup3ButtonSave": {
                        "placement": "save",
                        "parentid": "tabsGroup3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tabsGroup3ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tabsGroup3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    }
                }
            }
        );
    });

module.exports = router;
