/**
 * Created by mancr on 05/12/2016.
 */
var express = require('express');

// Create a router object from the top level express object for performing middleware and routing functions.
var router = express.Router();

// Good practice to use route method to avoid duplicate route naming and thus typo errors
router.route('/frontpage')
// Handler function (middleware system) for get request
    .get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "aboveCenter",
                            "center",
                            "east",
                            "south",
                            "underCenter"
                        ]
                    },
                    "toolbar-id": {
                        "parentid": "home",
                        "placement": "north",
                        "type": "toolbar",
                        "data": {
                            "logo": [
                                {
                                    "id": "logo-placement",
                                    "float": "left"
                                },
                                {
                                    "id": "logout-placement",
                                    "float": "right"
                                },
                                {
                                    "id": "redeploy-placement",
                                    "float": "right"
                                },
                                {
                                    "id": "label-placement",
                                    "float": "right"
                                }
                            ],
                            "tools": [
                                {
                                    "id": "tool-right-4",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-3",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-2",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-1",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-left-1",
                                    "float": "left"
                                }
                            ]
                        }
                    },
                    "logo": {
                        "parentid": "toolbar-id",
                        "placement": "logo-placement",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/images/logo-full.png"
                        }
                    },
                    "logout-button": {
                        "parentid": "toolbar-id",
                        "placement": "logout-placement",
                        "type": "button",
                        "data": {
                            "text": "Logout",
                            "type": "primary"
                        }
                    },
                    "redeploy-button": {
                        "parentid": "toolbar-id",
                        "placement": "redeploy-placement",
                        "type": "button",
                        "data": {
                            "text": "Redeploy",
                            "type": "primary"
                        }
                    },
                    "notification-label": {
                        "parentid": "toolbar-id",
                        "placement": "label-placement",
                        "type": "label",
                        "data": {
                            "text": "Not connected to Uniconta. Go to <b><a href= customerConfig.xhtml  target='_self'>Settings</a></b> to connect.",
                            "type": "error"
                        }
                    },
                    "label-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-1",
                        "type": "label",
                        "data": {
                            "text": "Cristiana Man",
                            "serverAction": "frontpage/edillion"
                        }
                    },
                    "icon-1-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-2",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/home-new-white.png",
                            "serverAction": "/frontpage"
                        }
                    },
                    "dropdown-panel": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-3",
                        "type": "dropdown-panel",
                        "data": {
                            "trigger": "trigger-container",
                            "overlay": "overlay-container"
                        }
                    },
                    "icon-2-id": {
                        "parentid": "dropdown-panel",
                        "placement": "trigger-container",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/customer-white.png"
                        }
                    },
                    "layout-vertical": {
                        "parentid": "dropdown-panel",
                        "placement": "overlay-container",
                        "type": "layout-vertical",
                        "data": [
                            "english-container",
                            "danish-container",
                            "password-container"
                        ]
                    },
                    "icon-english": {
                        "parentid": "layout-vertical",
                        "placement": "english-container",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/flag_gb.jpg"
                        }
                    },
                    "icon-danish": {
                        "parentid": "layout-vertical",
                        "placement": "danish-container",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/flag_dk.jpg"
                        }
                    },
                    "label-password": {
                        "parentid": "layout-vertical",
                        "placement": "password-container",
                        "type": "label",
                        "data": {
                            "text": "Change Password",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "icon-3-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-4",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/setup-white.png",
                            "serverAction": "/frontpage/accountSetup"
                        }
                    },
                    "tiles-grid": {
                        "parentid": "home",
                        "placement": "center",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "inbox",
                                "approval",
                                "outbox",
                                "documents",
                                "products.scripts",
                                "journal"
                            ],
                            "columns": 3
                        }
                    },
                    "marketing-tile": {
                        "parentid": "home",
                        "placement": "underCenter",
                        "type": "marketing-tile",
                        "data": {
                            "title": "Welcome to edilion",
                            "message": "You can post documents in the following ways:<br>- drop files to upload in the bottom right corner<br>- send a mail with the document attached, to: novo-nordisk_test_honorarfond_6614@edillion.net<br>- through the app 'edilion', which can be downloaded for Android from Google Play or iPhone from App Store<br>...<br>1. A brief moment after the document has been posted it will appear in the inbox<br>2. When the document has been read, it is sent for approval<br>Note, that it sometimes takes longer with reading a document, due to manual reading or quality assurance<br>3. When you or a collegue approves the document, it is sent to the outbox and onwards to the ERP system ",
                            "columns": 3
                        }
                    },
                    "inbox": {
                        "parentid": "tiles-grid",
                        "placement": "inbox",
                        "type": "tile",
                        "data": {
                            "header": "Inbox",
                            "message": "0 items in treatment",
                            "icon": "https://test.edillion.dk/faces/images/inbox.png",
                            "serverAction": "/primefaces/inbox"
                        }
                    },
                    "approval": {
                        "parentid": "tiles-grid",
                        "placement": "approval",
                        "type": "tile",
                        "data": {
                            "header": "Approval",
                            "message": "0 items for approval",
                            "icon": "https://test.edillion.dk/faces/images/approval.png",
                            "serverAction": "/primefaces/approval"
                        }
                    },
                    "outbox": {
                        "parentid": "tiles-grid",
                        "placement": "outbox",
                        "type": "tile",
                        "data": {
                            "header": "Outbox",
                            "message": "0 entries sent today",
                            "icon": "https://test.edillion.dk/faces/images/outbox.png",
                            "serverAction": "/primefaces/outbox"
                        }
                    },
                    "documents": {
                        "parentid": "tiles-grid",
                        "placement": "documents",
                        "type": "tile",
                        "data": {
                            "header": "Documents",
                            "message": "SYSTEM ADMIN TILE",
                            "icon": "https://test.edillion.dk/faces/images/documents.png",
                            "serverAction": "/primefaces/documents"
                        }
                    },
                    "products.scripts": {
                        "parentid": "tiles-grid",
                        "placement": "products.scripts",
                        "type": "tile",
                        "data": {
                            "header": "Scripts",
                            "message": "",
                            "icon": "https://test.edillion.dk/faces/images/scripts.png",
                            "serverAction": "/primefaces/scripts"
                        }
                    },
                    "journal": {
                        "parentid": "tiles-grid",
                        "placement": "journal",
                        "type": "tile",
                        "data": {
                            "header": "Journal",
                            "message": "",
                            "icon": "https://test.edillion.dk/faces/images/editDocument.png",
                            "serverAction": "/primefaces/journal"
                        }
                    },
                    "uploader-id": {
                        "parentid": "home",
                        "placement": "east",
                        "type": "upload",
                        "data": {
                            "message": "Drop your files here",
                            "serverAction": "upload"
                        }
                    }
                }
            }
        );
    });

router.route('/documents')
// Handler function (middleware system) for get request
    .get(function(req, res) {
        res.status(200);
        res.json(

        );
    });

router.route('/vendors/24688')
// Handler function (middleware system) for get request
    .get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "aboveCenter",
                            "center",
                            "east",
                            "south",
                            "underCenter"
                        ]
                    },
                    "toolbar-id": {
                        "parentid": "home",
                        "placement": "north",
                        "type": "toolbar",
                        "data": {
                            "logo": [
                                {
                                    "id": "logo-placement",
                                    "float": "left"
                                },
                                {
                                    "id": "logout-placement",
                                    "float": "right"
                                },
                                {
                                    "id": "label-placement",
                                    "float": "right"
                                }
                            ],
                            "tools": [
                                {
                                    "id": "tool-right-4",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-3",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-2",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-1",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-left-1",
                                    "float": "left"
                                }
                            ]
                        }
                    },
                    "logo": {
                        "parentid": "toolbar-id",
                        "placement": "logo-placement",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/images/logo-full.png",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "logout-button": {
                        "parentid": "toolbar-id",
                        "placement": "logout-placement",
                        "type": "button",
                        "data": {
                            "text": "Logout",
                            "type": "primary"
                        }
                    },
                    "e-conomic-label": {
                        "parentid": "toolbar-id",
                        "placement": "label-placement",
                        "type": "label",
                        "data": {
                            "text": "Not connected to e-conomic!",
                            "type": "alert",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "label-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-1",
                        "type": "label",
                        "data": {
                            "text": "Cristiana Man",
                            "serverAction": "frontpage/edillion"
                        }
                    },
                    "icon-1-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-2",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/home-new-white.png",
                            "serverAction": "/frontpage"
                        }
                    },
                    "icon-2-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-3",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/customer-white.png"
                        }
                    },
                    "icon-3-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-4",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/setup-white.png",
                            "serverAction": "/frontpage/accountSetup"
                        }
                    },
                    "vendorPaginator": {
                        "placement": "underCenter",
                        "parentid": "home",
                        "type": "paginator",
                        "data": {
                            "page": 3,
                            "perPage": 5,
                            "items": 58
                        }
                    },
                    "vendorTable": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "table",
                        "data": {
                            "columns": [
                                {
                                    "index": 0,
                                    "header": "Create",
                                    "sort": "asc",
                                    "show": true
                                },
                                {
                                    "index": 1,
                                    "header": "Create By",
                                    "show": false
                                },
                                {
                                    "index": 2,
                                    "header": "Modified",
                                    "show": true
                                },
                                {
                                    "index": 3,
                                    "header": "Modified By",
                                    "show": true
                                },
                                {
                                    "index": 4,
                                    "header": "Name",
                                    "show": true
                                },
                                {
                                    "index": 5,
                                    "header": "Number",
                                    "show": true
                                },
                                {
                                    "index": 6,
                                    "header": "VAT Number",
                                    "show": true
                                },
                                {
                                    "index": 7,
                                    "header": "Email",
                                    "show": true
                                },
                                {
                                    "index": 8,
                                    "header": "Country",
                                    "show": true
                                },
                                {
                                    "index": 9,
                                    "header": "Sync status",
                                    "show": true
                                }
                            ],
                            "rows": [
                                {
                                    "id": "24676",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "BO HANQUIST CONVENIENCE ApS",
                                        "25063988",
                                        "DK25063988",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24679",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "MULTILINE A/S",
                                        "10665841",
                                        "DK10665841",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24688",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "GENZ KØD A/S",
                                        "15252294",
                                        "DK15252294",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24697",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "BC CATERING ROSKILDE A/S",
                                        "19354598",
                                        "DK19354598",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24696",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Svane-Ren",
                                        "33673450",
                                        "DK33673450",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24677",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "FLEXYBOX ApS",
                                        "26428947",
                                        "DK26428947",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24693",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "SØHUSET & MUNDT ApS",
                                        "33492499",
                                        "33492499",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24698",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Urban Street Food IVS",
                                        "37582883",
                                        "DK37582883",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24695",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "BENT BRANDT A/S",
                                        "37238910",
                                        "DK37238910",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24689",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "H.J. HANSEN VIN A/S",
                                        "89973511",
                                        "DK89973511",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24700",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "ISS",
                                        "660226314",
                                        "660226314",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24680",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "søgade",
                                        "1",
                                        "45464",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24702",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "NHL DATA ApS",
                                        "25575555",
                                        "DK25575555",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24682",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "MIDTJYSK AV TEKNIK ApS",
                                        "26011361",
                                        "DK26011361",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24705",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "REA Elektronik Gmbh",
                                        "615463800",
                                        "DE111656563",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24690",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Mybanker.dk A/S",
                                        "30504496",
                                        "DK30504496",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24678",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "CHARLES GULVE ENGROS A/S",
                                        "26421519",
                                        "DK26421519",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24683",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "SOFTILITY ApS",
                                        "25064526",
                                        "25064526",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24694",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "IMPRO PRINTING LTD",
                                        "208509807",
                                        "208509807",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24692",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "THE ODYSSEY TRUST COMPANY LTD",
                                        "714136856",
                                        "714136856",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24681",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "KUMA A/S",
                                        "20257377",
                                        "DK20257377",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24686",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "AthGene",
                                        "800190706",
                                        "800190706",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24691",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Noonan Services Group (Nl) Ltd",
                                        "923091541",
                                        "GB923091541",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24703",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "ISS Technical Services",
                                        "12456789",
                                        "",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24684",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "HKI OST ApS",
                                        "25447123",
                                        "DK25447123",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24704",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "NGI",
                                        "20861770",
                                        "20861770",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24706",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "GREENS ENGROS ApS",
                                        "28996063",
                                        "DK28996063",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24699",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Plant2Plast v/Søren Kirkegård     Nielsen",
                                        "29580898",
                                        "DK29580898",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24701",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "GLENN WOODS",
                                        "575700432",
                                        "GB575700432",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24685",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Sunesen Consulting v/Peter Sunesen",
                                        "33570562",
                                        "DK33570562",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24687",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "LAKRIDS BY JOHAN BÜLOW A/S",
                                        "33041586",
                                        "DK33041586",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                },
                                {
                                    "id": "24707",
                                    "cells": [
                                        "2016-02-17 15:12:28.684",
                                        "flavium_no@yahoo.com",
                                        "2016-03-09 09:54:39.573",
                                        "flavium_no@yahoo.com",
                                        "Berqhote 1",
                                        "3434534",
                                        "3434534",
                                        "flavium_no@yahoo.com",
                                        "DK",
                                        "sync"
                                    ]
                                }
                            ],
                            "callback": "vendors"
                        }
                    },
                    "tabs": {
                        "placement": "east",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "Details",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Address"
                                },
                                {
                                    "id": "tab3",
                                    "title": "Payment"
                                }
                            ],
                            "options": {
                                "alignment": "left"
                            }
                        }
                    },
                    "tab1vl": {
                        "placement": "tab1",
                        "parentid": "tabs",
                        "type": "layout-vertical",
                        "data": [
                            "property-editor",
                            "submit",
                            "cancel"
                        ]
                    },
                    "propertyEditor1Tab1": {
                        "placement": "property-editor",
                        "parentid": "tab1vl",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Name",
                                    "value": "GENZ KØD A/S",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                },
                                {
                                    "id": "number",
                                    "label": "Number",
                                    "value": "15252294",
                                    "type": "number",
                                    "default": "",
                                    "legalValues": []
                                },
                                {
                                    "id": "VATnumber",
                                    "label": "VAT Number",
                                    "value": "DK15252294",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                },
                                {
                                    "id": "Email",
                                    "label": "Email",
                                    "value": "flavium_no@yahoo.com",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                },
                                {
                                    "id": "Country",
                                    "label": "Country",
                                    "value": "DK",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                },
                                {
                                    "id": "isAwesome",
                                    "label": "Sync",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": []
                                }
                            ]
                        }
                    },
                    "button-1-tab1": {
                        "placement": "submit",
                        "parentid": "tab1vl",
                        "type": "button",
                        "data": {
                            "text": "Submit",
                            "type": "primary",
                            "serverAction": {
                                "method": "POST",
                                "endpoint": "/api/vendors",
                                "output": "{ name: \"<$.components.propertyEditor1Tab1.data.fields[0].value>\"}"
                            }
                        }
                    },
                    "button-2-tab1": {
                        "placement": "cancel",
                        "parentid": "tab1vl",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default",
                            "serverAction": ""
                        }
                    }
                }
            }
        );
    });

router.route('/configuration')
// Handler function (middleware system) for get request
    .get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "home": {
                        "parentid": "root",
                        "placement": "west",
                        "type": "layout",
                        "data": [
                            "north",
                            "west",
                            "aboveCenter",
                            "center",
                            "east",
                            "south",
                            "underCenter"
                        ]
                    },
                    "toolbar-id": {
                        "parentid": "home",
                        "placement": "north",
                        "type": "toolbar",
                        "data": {
                            "logo": [
                                {
                                    "id": "logo-placement",
                                    "float": "left"
                                },
                                {
                                    "id": "logout-placement",
                                    "float": "right"
                                },
                                {
                                    "id": "label-placement",
                                    "float": "right"
                                }
                            ],
                            "tools": [
                                {
                                    "id": "tool-right-4",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-3",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-2",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-right-1",
                                    "float": "right"
                                },
                                {
                                    "id": "tool-left-1",
                                    "float": "left"
                                }
                            ]
                        }
                    },
                    "logo": {
                        "parentid": "toolbar-id",
                        "placement": "logo-placement",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/images/logo-full.png",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "logout-button": {
                        "parentid": "toolbar-id",
                        "placement": "logout-placement",
                        "type": "button",
                        "data": {
                            "text": "Logout",
                            "type": "primary"
                        }
                    },
                    "e-conomic-label": {
                        "parentid": "toolbar-id",
                        "placement": "label-placement",
                        "type": "label",
                        "data": {
                            "text": "Not connected to e-conomic!",
                            "type": "alert",
                            "externalAction": "https://google.dk"
                        }
                    },
                    "label-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-1",
                        "type": "label",
                        "data": {
                            "text": "Cristiana Man",
                            "serverAction": "frontpage/edillion"
                        }
                    },
                    "icon-1-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-2",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/home-new-white.png",
                            "serverAction": "/frontpage"
                        }
                    },
                    "icon-2-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-3",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/customer-white.png"
                        }
                    },
                    "icon-3-id": {
                        "parentid": "toolbar-id",
                        "placement": "tool-right-4",
                        "type": "image",
                        "data": {
                            "image": "https://test.edillion.dk/faces/images/setup-white.png",
                            "serverAction": "/frontpage/accountSetup"
                        }
                    },
                    "menu-table": {
                        "placement": "west",
                        "parentid": "home",
                        "type": "table",
                        "data": {
                            "columns": [
                                {
                                    "index": 0,
                                    "header": "",
                                    "sort": "asc",
                                    "show": true
                                }
                            ],
                            "rows": [
                                {
                                    "id": "master",
                                    "cells": [
                                        "Master data"
                                    ]
                                },
                                {
                                    "id": "product",
                                    "cells": [
                                        "Product configuration"
                                    ]
                                },
                                {
                                    "id": "e-conomic",
                                    "cells": [
                                        "e-conomic"
                                    ]
                                },
                                {
                                    "id": "accounting",
                                    "cells": [
                                        "Accounting"
                                    ]
                                },
                                {
                                    "id": "approval",
                                    "cells": [
                                        "Approval"
                                    ],
                                    "selected": true
                                },
                                {
                                    "id": "company",
                                    "cells": [
                                        "Company"
                                    ]
                                },
                                {
                                    "id": "journal",
                                    "cells": [
                                        "Journal"
                                    ]
                                },
                                {
                                    "id": "customer",
                                    "cells": [
                                        "Customer attributes"
                                    ]
                                },
                                {
                                    "id": "newcustomer",
                                    "cells": [
                                        "New customer"
                                    ]
                                }
                            ],
                            "options": {
                                "selectMode": "single",
                                "showHeader": false
                            },
                            "callback": "gfe/configuration"
                        }
                    },
                    "tabs": {
                        "placement": "center",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "Settings",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Groups"
                                },
                                {
                                    "id": "tab3",
                                    "title": "Create Group"
                                }
                            ],
                            "options": {
                                "alignment": "left"
                            }
                        }
                    },
                    "tab1Layout": {
                        "placement": "tab1",
                        "parentid": "tabs",
                        "type": "layout-vertical",
                        "data": [
                            "label1",
                            "generalProperty",
                            "label2",
                            "documentsProperty",
                            "label3",
                            "defaultsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tab1Label1": {
                        "parentid": "tab1Layout",
                        "placement": "label1",
                        "type": "label",
                        "data": {
                            "text": "General"
                        }
                    },
                    "tab1Label2": {
                        "parentid": "tab1Layout",
                        "placement": "label2",
                        "type": "label",
                        "data": {
                            "text": "Downloads"
                        }
                    },
                    "tab1Label3": {
                        "parentid": "tab1Layout",
                        "placement": "label3",
                        "type": "label",
                        "data": {
                            "text": "Group Defaults"
                        }
                    },
                    "tab1PropertyEditor1": {
                        "placement": "generalProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Enable approval flow",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Approval flow package",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false,
                                    "help": "This is some message."
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1PropertyEditor2": {
                        "placement": "documentsProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Account required",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Payment means required",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "name",
                                    "label": "Pressure Payment means",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1PropertyEditor3": {
                        "placement": "defaultsProperty",
                        "parentid": "tab1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "field1",
                                    "label": "Forward new documents to",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false,
                                    "help": "This is some message."
                                },
                                {
                                    "id": "field2",
                                    "label": "Forward new documents with problms to",
                                    "value": "1",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field3",
                                    "label": "Default book keeper group",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "Group 1",
                                            "value": "2"
                                        },
                                        {
                                            "label": "Group 2",
                                            "value": "3"
                                        },
                                        {
                                            "label": "Group 3",
                                            "value": "4"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field4",
                                    "label": "Default superior group",
                                    "value": "3",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "Group 1",
                                            "value": "2"
                                        },
                                        {
                                            "label": "Group 2",
                                            "value": "3"
                                        },
                                        {
                                            "label": "Group 3",
                                            "value": "4"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field5",
                                    "label": "Forward rejected documents to",
                                    "value": "2",
                                    "type": "select",
                                    "default": "",
                                    "legalValues": [
                                        {
                                            "label": "None",
                                            "value": "1"
                                        },
                                        {
                                            "label": "cma@edilion.net",
                                            "value": "2"
                                        }
                                    ],
                                    "isDisabled": false
                                },
                                {
                                    "id": "field6",
                                    "label": "Default duration",
                                    "value": "22",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab1ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tab1Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tab1ButtonSave": {
                        "placement": "save",
                        "parentid": "tab1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tab1ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tab1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    },
                    "tab3Layout": {
                        "placement": "tab3",
                        "parentid": "tabs",
                        "type": "layout-vertical",
                        "data": [
                            "createProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tab3PropertyEditor3": {
                        "placement": "createProperty",
                        "parentid": "tab3Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "t3field1",
                                    "label": "Group name",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": []
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tab3ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tab3Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "create"
                            ],
                            "columns": 1
                        }
                    },
                    "tab3ButtonCreate": {
                        "placement": "create",
                        "parentid": "tab3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Create",
                            "type": "primary"
                        }
                    },
                    "tab2Table": {
                        "placement": "tab2",
                        "parentid": "tabs",
                        "type": "table",
                        "data": {
                            "options": {
                                "selectMode": "single",
                                "showHeader": true
                            },
                            "columns": [
                                {
                                    "index": 0,
                                    "header": "Name",
                                    "sort": "asc",
                                    "show": true
                                },
                                {
                                    "index": 1,
                                    "header": "Members",
                                    "sort": "asc",
                                    "show": false
                                },
                                {
                                    "index": 2,
                                    "header": "Approval limit",
                                    "sort": "asc",
                                    "show": true
                                },
                                {
                                    "index": 3,
                                    "header": "Superior",
                                    "sort": "asc",
                                    "show": true
                                }
                            ],
                            "rows": [
                                {
                                    "id": "1",
                                    "cells": [
                                        "Marketing",
                                        "Peter",
                                        "10.000",
                                        "CEO"
                                    ]
                                },
                                {
                                    "id": "2",
                                    "cells": [
                                        "Development",
                                        "Cristiana, Simon, Mathias, Flaviu",
                                        "5.000",
                                        "CEO"
                                    ]
                                },
                                {
                                    "id": "3",
                                    "cells": [
                                        "CEO",
                                        "Christian",
                                        "No limit",
                                        "No superior"
                                    ]
                                }
                            ],
                            "callback": "gfe/configuration/approval/group"
                        }
                    }
                }
            }
        );
    });

router.route('/configuration/group/1')
// Handler function (middleware system) for get request
    .get(function(req, res) {
        res.status(200);
        res.json(
            {
                "components": {
                    "tabsGroup": {
                        "placement": "east",
                        "parentid": "home",
                        "type": "tabs",
                        "data": {
                            "tabs": [
                                {
                                    "id": "tab1",
                                    "title": "settings",
                                    "active": true
                                },
                                {
                                    "id": "tab2",
                                    "title": "Members"
                                },
                                {
                                    "id": "tab3",
                                    "title": "Rights"
                                }
                            ],
                            "options": {
                                "alignment": "center"
                            }
                        }
                    },
                    "tabsGroup1Layout": {
                        "placement": "tab1",
                        "parentid": "tabsGroup",
                        "type": "layout-vertical",
                        "data": [
                            "settingsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tabsGroup1PropertyEditor": {
                        "placement": "settingsProperty",
                        "parentid": "tabsGroup1Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "name",
                                    "label": "Name",
                                    "value": "Marketing",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false,
                                    "help": "This is a help message"
                                },
                                {
                                    "id": "project",
                                    "label": "Project",
                                    "value": "Some Fancy Name",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "department",
                                    "label": "Department",
                                    "value": "Marketing",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "bookkeeper",
                                    "label": "Bookkeeper",
                                    "value": "",
                                    "type": "text",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "assignproject",
                                    "label": "Auto-assign received docs to project",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "assigndepartment",
                                    "label": "Auto-assign received docs to department",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": false
                            }
                        }
                    },
                    "tabsGroup1ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tabsGroup1Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup1ButtonSave": {
                        "placement": "save",
                        "parentid": "tabsGroup1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tabsGroup1ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tabsGroup1ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    },
                    "tabsGroup2Grid": {
                        "placement": "tab2",
                        "parentid": "tabsGroup",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "label1",
                                "removeButton1",
                                "label2",
                                "removeButton2",
                                "label3",
                                "removeButton3",
                                "addMemberButton"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup2Label1": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label1",
                        "type": "label",
                        "data": {
                            "text": "Simon"
                        }
                    },
                    "tabsGroup2Button1": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton1",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2Label2": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label2",
                        "type": "label",
                        "data": {
                            "text": "Mathias"
                        }
                    },
                    "tabsGroup2Button2": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton2",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2Label3": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "label3",
                        "type": "label",
                        "data": {
                            "text": "Flaviu"
                        }
                    },
                    "tabsGroup2Button3": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "removeButton3",
                        "type": "button",
                        "data": {
                            "text": "Remove",
                            "type": "primary"
                        }
                    },
                    "tabsGroup2ButtonAddMember": {
                        "parentid": "tabsGroup2Grid",
                        "placement": "addMemberButton",
                        "type": "button",
                        "data": {
                            "text": "Add member",
                            "type": "primary"
                        }
                    },
                    "tabsGroup3Layout": {
                        "placement": "tab3",
                        "parentid": "tabsGroup",
                        "type": "layout-vertical",
                        "data": [
                            "rightsProperty",
                            "buttonsGrid"
                        ]
                    },
                    "tabsGroup3PropertyEditor": {
                        "placement": "rightsProperty",
                        "parentid": "tabsGroup3Layout",
                        "type": "property-editor",
                        "data": {
                            "fields": [
                                {
                                    "id": "forward",
                                    "label": "Forward",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "postpone",
                                    "label": "Postpone",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "bookkeeper",
                                    "label": "Send to bookkeeper",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "approve1",
                                    "label": "Approve (is paid)",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "approve2",
                                    "label": "Approve (to be paid)",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit1",
                                    "label": "Edit Account",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit2",
                                    "label": "Edit payment means",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit3",
                                    "label": "Edit project",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "edit4",
                                    "label": "Edit department",
                                    "value": false,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                },
                                {
                                    "id": "reject",
                                    "label": "Reject",
                                    "value": true,
                                    "type": "boolean",
                                    "default": "",
                                    "legalValues": [],
                                    "isDisabled": false
                                }
                            ],
                            "options": {
                                "isHorizontal": true
                            }
                        }
                    },
                    "tabsGroup3ButtonsGrid": {
                        "placement": "buttonsGrid",
                        "parentid": "tabsGroup3Layout",
                        "type": "grid",
                        "data": {
                            "containers": [
                                "save",
                                "cancel"
                            ],
                            "columns": 2
                        }
                    },
                    "tabsGroup3ButtonSave": {
                        "placement": "save",
                        "parentid": "tabsGroup3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Save",
                            "type": "primary"
                        }
                    },
                    "tabsGroup3ButtonCancel": {
                        "placement": "cancel",
                        "parentid": "tabsGroup3ButtonsGrid",
                        "type": "button",
                        "data": {
                            "text": "Cancel",
                            "type": "default"
                        }
                    }
                }
            }
        );
    });


module.exports = router;
