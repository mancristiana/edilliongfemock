// Creates an express application
var express = require('express');
var app = express(); // Top-level function exported by the express module

var bodyParser = require('body-parser');


var api = require('./routes/api.js');
var globetrotter = require('./routes/globetrotter.js');
var gfe = require('./routes/gfe.js');

// CORS Cross-Origin Resource Sharing
app.all('/*', function (req, res, next) {
	res.header("Access-Control-Allow-Origin", req.header("Origin"));
	res.header("Access-Control-Allow-Credentials", "true");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    res.header("Access-Control-Max-Age", "86400");
    res.header("Allow", "GET, HEAD, POST, TRACE, OPTIONS");
	next();
});

// Use middleware which serves files from given 'public' directory
app.use(express.static('public'));


// Use body-parsing middleware for JSON like experience with URL-encoded
// Extended syntax uses qs library (when true) and querystring library (when false)
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.all(function(error, req, res, next) {
	 //Catch bodyParser error
    if (error.message === "invalid json") {
        res.status(400).send({ "error": "400 <br>Wrongly formated <code>json</code> was sent" });
    } else {
        next();
    }
});

// For specified path use required modules
app.use('/api/', api);
app.use('/api/gui/globetrotter', globetrotter);
app.use('/api/gui', gfe);

app.listen(8033);
