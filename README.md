# Mock Service for GFE

## Instalation

Get the source code

`git clone https://mancristiana@bitbucket.org/mancristiana/fitnessappnodejs.git
`

Make sure you have node.js installed

`npm install` Installs the node_modules 

## Usage

`node index.js` Runs the server

## In GFE
 Make sure that your config.json has the apiUrl set to the MockRequester


```
{
  "apiUrl" : "http://localhost:8033/api/",
  "initialEndpoint" : "globetrotter/editexpense"
}
```
